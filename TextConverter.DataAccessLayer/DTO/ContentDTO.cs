﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TextConverter.DataAccessLayer.DTO
{
    [Serializable]
    public class ContentDTO
    {
        [Required]
        public string Text { get; set; }
    }
}
