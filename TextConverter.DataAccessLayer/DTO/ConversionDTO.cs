﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextConverter.DataAccessLayer.DTO
{
    [Serializable]
    public class ConversionDTO
    {
        public string Csv { get; set; }
        public string Xml { get; set; }
    }
}
