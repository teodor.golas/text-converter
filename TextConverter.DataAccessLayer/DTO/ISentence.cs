﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextConverter.DataAccessLayer.DTO
{
    public interface ISentence
    {
        public List<SentenceDTO> Sentences { get; set; }
    }
}
