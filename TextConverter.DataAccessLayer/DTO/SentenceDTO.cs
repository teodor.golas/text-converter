﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace TextConverter.DataAccessLayer.DTO
{
    [Serializable]
    [XmlRoot(ElementName = "sentence")]
    public class SentenceDTO
    {
        public SentenceDTO()
        {
            Words = new List<string>();
        }
        [XmlIgnore]
        public string Name { get; set; }
        [XmlElement(ElementName = "word")]
        public List<string> Words { get; set; }
    }
}
