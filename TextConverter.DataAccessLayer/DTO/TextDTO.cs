﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Serialization;

namespace TextConverter.DataAccessLayer.DTO
{
    [Serializable]
    [XmlRoot(ElementName = "text")]
    public class TextDTO : ISentence
    {
        public TextDTO()
        {
            Sentences = new List<SentenceDTO>();
        }
        [Required]
        [XmlElement(ElementName = "sentence")]
        public List<SentenceDTO> Sentences { get; set; }

    }
}
