﻿namespace TextConverter.DataAccessLayer.Enums
{
    public static class Enumeration
    {
        /// <summary>
        /// Action results
        /// </summary>
        public enum ResultMessage
        {
            ActionSuccess = 0,
            ActionFailed = 1,
            UnexpectedServerError = 2,
        }

        /// <summary>
        /// Word splitting delimiters
        /// </summary>
        public static string[] SplitSymbols = new[]
        {
            ",",
            ";",
            " ",
            "!",
            "'",
            ":",
            "?",
            "\"",
            "’",
            "„",
            "”",
            "‘",
            "‟",
            "″"
        };
    }
}
