﻿using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextConverter.DataAccessLayer.DTO;

namespace TextConverter.Infrastructure.Converters
{
    /// <summary>
    /// Converts class to csv format
    /// </summary>
    public class CsvConverter : IConverter
    {
        public string Name => nameof(CsvConverter);
        private readonly CultureInfo culture = CultureInfo.InvariantCulture;
        public async Task<string> RunAsync<T>(T input, bool? addHeader = null, ClassMap map = null) where T : ISentence
        {
            var csv = new StringBuilder();
            await using var textWriter = new StringWriter(csv);
            using var csvWriter = new CsvWriter(textWriter, culture);
            //configuration
            csvWriter.Configuration.RegisterClassMap(map);
            csvWriter.Configuration.Delimiter = ", ";
            csvWriter.Configuration.ShouldQuote = (field, ctx) => false;
            csvWriter.Configuration.NewLine = NewLine.Environment;

            // we want to have a header row (optional?)
            if (addHeader != null && (bool)addHeader)
            {
                string header = string.Join(", ", input.Sentences.SelectMany(s => s.Words).ToArray().OrderBy(s => s));
                csvWriter.WriteConvertedField(header);
                await csvWriter.NextRecordAsync();
            }

            // write our records
            foreach (var value in input.Sentences)
            {
                csvWriter.WriteRecord(value);
                await csvWriter.NextRecordAsync();
            }

            // make sure all records are flushed to stream
            await csvWriter.FlushAsync();

            return csv.ToString();
        }
    }
}
