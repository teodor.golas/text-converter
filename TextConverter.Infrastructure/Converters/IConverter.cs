﻿using CsvHelper.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TextConverter.DataAccessLayer.DTO;

namespace TextConverter.Infrastructure.Converters
{
    public interface IConverter
    {
        string Name { get; }
        Task<string> RunAsync<T>(T input, bool? addHeader = null, ClassMap map = null) where T : ISentence;
    }
}
