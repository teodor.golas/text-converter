﻿using CsvHelper.Configuration;
using TextConverter.DataAccessLayer.DTO;

namespace TextConverter.Infrastructure.Converters.Maps
{
    public sealed class SentenceMap : ClassMap<SentenceDTO>
    {
        public SentenceMap()
        {
            Map(m => m.Name);
            Map(m => m.Words);
        }
    }
}
