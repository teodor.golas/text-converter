﻿using CsvHelper.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TextConverter.DataAccessLayer.DTO;

namespace TextConverter.Infrastructure.Converters
{
    /// <summary>
    /// Converts class to xml format
    /// </summary>
    public class XmlConverter : IConverter
    {
        public string Name => nameof(XmlConverter);
        public async Task<string> RunAsync<T>(T input, bool? addHeader, ClassMap map = null) where T : ISentence
        {
            var serializer = new XmlSerializer(typeof(T));
            //omitt namespaces
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            // we will write the our result to memory
            await using var stream = new MemoryStream();
            // the string will be utf8 encoded
            await using var writer = new StreamWriter(stream, Encoding.UTF8);

            // here we go!
            serializer.Serialize(writer, input, ns);

            // flush our write to make sure its all there
            await writer.FlushAsync();

            // reset the stream back to 0
            stream.Position = 0;

            using var reader = new StreamReader(stream);

            // we reread the stream to a string
            var xml = await reader.ReadToEndAsync();

            return xml;
        }
    }
}
