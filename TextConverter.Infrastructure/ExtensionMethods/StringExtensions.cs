﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace TextConverter.Infrastructure.ExtensionMethods
{
    public static class StringExtensions
    {
        public static bool IsAlphanumeric(this string value)
        {
            return Regex.IsMatch(value, @"^[a-zA-Z]+|[\u4e00-\u9fa5]+$");
        }
    }
}
