﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextConverter.Services.Helpers
{
    public class BaseResponse
    {
        public BaseResponse(string message, int resultCode)
        {
            ResultMessage = message;
            StatusCode = resultCode;
        }
        public int StatusCode { get; set; }
        public string ResultMessage { get; set; }
    }
}
