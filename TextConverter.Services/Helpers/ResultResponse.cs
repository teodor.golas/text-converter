﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextConverter.Services.Helpers
{
    public class ResultResponse<T> : BaseResponse
    {
        public ResultResponse(string message, int resultCode, T data) : base(message, resultCode)
        {
            Data = data;
        }
        public T Data { get; set; }
    }
}
