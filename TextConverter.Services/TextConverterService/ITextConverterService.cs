﻿using System;
using System.Threading.Tasks;
using TextConverter.DataAccessLayer.DTO;
using TextConverter.Services.Helpers;

namespace TextConverter.Services.TextConverterService
{
    public interface ITextConverterService
    {
        Task<ResultResponse<ConversionDTO>> ConvertAsync(ContentDTO text);
    }
}
