﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TextConverter.DataAccessLayer.DTO;
using TextConverter.DataAccessLayer.Enums;
using TextConverter.Infrastructure.Converters;
using TextConverter.Infrastructure.Converters.Maps;
using TextConverter.Services.Helpers;

namespace TextConverter.Services.TextConverterService
{
    /// <summary>
    /// Handles text processing
    /// </summary>
    public class TextConverterService : ITextConverterService
    {
        #region privates
        private IEnumerable<IConverter> _converters { get; set; }
        private readonly ILogger _logger;
        #endregion
        //inject login provider and relevant interfaces
        #region ctor
        public TextConverterService(IEnumerable<IConverter> converters, ILoggerFactory loggerFactory)
        {
            _converters = converters ?? throw new ArgumentNullException($"{nameof(converters)} is null. Did you forget to register it in DI container?");
            _logger = loggerFactory.CreateLogger(this.GetType());
        }
        #endregion
        #region public
        /// <summary>
        /// Converts input text into csv and xml format
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public async Task<ResultResponse<ConversionDTO>> ConvertAsync(ContentDTO content)
        {
            try
            {
                _logger.LogDebug($"START - {nameof(ConvertAsync)}");
                var text = new TextDTO();
                int counter = 0;
                var sentences = content.Text.Split('.');
                foreach (string sentence in sentences)
                {
                    var sentenceClass = new SentenceDTO();

                    var words = sentence.Split(Enumeration.SplitSymbols, StringSplitOptions.RemoveEmptyEntries);
                    if (words.Any())
                    {
                        sentenceClass.Name = string.Join(' ', "Sentence", (++counter).ToString());
                        sentenceClass.Words.AddRange(words.OrderBy((w) => w)); //you could add filters here like for ex. w.IsAlphanumeric()
                        text.Sentences.Add(sentenceClass);
                    }
                };

                //convert text
                var csv = _converters.FirstOrDefault(c => c.GetType().Name == nameof(CsvConverter)).RunAsync(text, true, new SentenceMap());
                var xml = _converters.FirstOrDefault(c => c.GetType().Name == nameof(XmlConverter)).RunAsync(text);
                await Task.WhenAll(csv, xml);

                var result = new ConversionDTO()
                {
                    Csv = csv.Result,
                    Xml = xml.Result
                };

                _logger.LogDebug($"FINISH - {nameof(ConvertAsync)}");

                return new ResultResponse<ConversionDTO>(Enumeration.ResultMessage.ActionSuccess.ToString(), StatusCodes.Status200OK, result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                //here you'll log error and handle different error scenarios to return accurate error message
                return new ResultResponse<ConversionDTO>(Enumeration.ResultMessage.ActionFailed.ToString(), StatusCodes.Status500InternalServerError, null);
            }
        }
        #endregion
    }
}
