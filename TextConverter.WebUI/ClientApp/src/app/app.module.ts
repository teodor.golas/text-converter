import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './pages/home/home.component';
import { RoutingModule } from './modules/routing.module';
import { TextConverterComponent } from './pages/text-converter/text-converter.component';
import { PrimeModule } from './modules/prime-module';
import { ShowErrorsComponent } from './components/show-errors/show-errors.component';
import { baseUrl } from './helpers/constants';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    TextConverterComponent,
    ShowErrorsComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RoutingModule,
    ReactiveFormsModule,
    PrimeModule,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: baseUrl, useValue: environment.baseUrl }],
  bootstrap: [AppComponent]
})
export class AppModule { }
