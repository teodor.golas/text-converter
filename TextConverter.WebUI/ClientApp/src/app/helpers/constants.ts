import { InjectionToken } from "@angular/core";

export const baseUrl = new InjectionToken<string>('baseUrl');
