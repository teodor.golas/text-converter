"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrimeModule = void 0;
var core_1 = require("@angular/core");
var menubar_1 = require("primeng/menubar");
var table_1 = require("primeng/table");
var toast_1 = require("primeng/toast");
var calendar_1 = require("primeng/calendar");
var inputtext_1 = require("primeng/inputtext");
var card_1 = require("primeng/card");
var button_1 = require("primeng/button");
var checkbox_1 = require("primeng/checkbox");
var radiobutton_1 = require("primeng/radiobutton");
var tabview_1 = require("primeng/tabview");
var password_1 = require("primeng/password");
var panel_1 = require("primeng/panel");
var inputtextarea_1 = require("primeng/inputtextarea");
var confirmdialog_1 = require("primeng/confirmdialog");
var api_1 = require("primeng/api");
var api_2 = require("primeng/api");
var message_1 = require("primeng/message");
var messages_1 = require("primeng/messages");
var toolbar_1 = require("primeng/toolbar");
var dropdown_1 = require("primeng/dropdown");
var togglebutton_1 = require("primeng/togglebutton");
var multiselect_1 = require("primeng/multiselect");
var galleria_1 = require("primeng/galleria");
var tabmenu_1 = require("primeng/tabmenu");
var orderlist_1 = require("primeng/orderlist");
var PrimeModule = /** @class */ (function () {
    function PrimeModule() {
    }
    PrimeModule = __decorate([
        core_1.NgModule({
            exports: [
                menubar_1.MenubarModule,
                table_1.TableModule,
                toast_1.ToastModule,
                calendar_1.CalendarModule,
                inputtext_1.InputTextModule,
                card_1.CardModule,
                button_1.ButtonModule,
                checkbox_1.CheckboxModule,
                radiobutton_1.RadioButtonModule,
                tabview_1.TabViewModule,
                password_1.PasswordModule,
                panel_1.PanelModule,
                inputtextarea_1.InputTextareaModule,
                confirmdialog_1.ConfirmDialogModule,
                messages_1.MessagesModule,
                message_1.MessageModule,
                toolbar_1.ToolbarModule,
                dropdown_1.DropdownModule,
                togglebutton_1.ToggleButtonModule,
                multiselect_1.MultiSelectModule,
                galleria_1.GalleriaModule,
                tabmenu_1.TabMenuModule,
                orderlist_1.OrderListModule
            ],
            providers: [api_1.ConfirmationService, api_2.MessageService]
        })
    ], PrimeModule);
    return PrimeModule;
}());
exports.PrimeModule = PrimeModule;
//# sourceMappingURL=prime-module.js.map