import { NgModule } from '@angular/core';
import { ToastModule } from 'primeng/toast';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { TabMenuModule } from 'primeng/tabmenu';
import { MessagesModule } from 'primeng/messages';
import { FieldsetModule } from 'primeng/fieldset';

@NgModule({
  exports: [
    ToastModule,
    CardModule,
    ButtonModule,
    InputTextareaModule,
    MessageModule,
    TabMenuModule,
    MessagesModule,
    FieldsetModule
  ],
  providers: [MessageService]
})
export class PrimeModule {
}
