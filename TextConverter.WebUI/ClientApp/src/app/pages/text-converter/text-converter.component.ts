import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Content } from '../../models/content';
import { Conversion } from '../../models/conversion';
import { ConverterService } from '../../serivces/converter.service';

@Component({
  selector: 'app-text-converter',
  templateUrl: './text-converter.component.html',
  styleUrls: ['./text-converter.component.scss']
})
export class TextConverterComponent implements OnInit {
  group: FormGroup;
  csv: string;
  xml: string;
  constructor(
    private readonly converterService: ConverterService,
    private messageService: MessageService) {
    this.group = this.createFormGroup();
  }

  ngOnInit() {
  }

  onSubmit() {
    let content = this.group.value as Content;
    this.converterService.create(content)
      .subscribe(value => {
        let result: Conversion = value;
        this.csv = result.csv;
        this.xml = result.xml;
        this.messageService.add({ severity: 'success', summary: 'SUCCESS', detail: "", life: 3000 });
      },
        err => {
          this.messageService.add({ severity: 'error', summary: 'FAILED', detail: err.error.detail, life: 3000 });
        });
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      text: new FormControl(null, [Validators.required, Validators.minLength(3)]),
    });
  }

  reset() {
    this.group.reset();
  }
}
