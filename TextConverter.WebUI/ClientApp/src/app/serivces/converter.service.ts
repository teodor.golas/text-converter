import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Content } from '../models/content';
import { Conversion } from '../models/conversion';
import { HttpClientService } from './http-client.service';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor(private _http: HttpClientService) { }

  private converterUrl: string = '/api/converter/';

  create(text: Content): Observable<Conversion> {
    return this._http.post<Conversion>(`${this.converterUrl}`, text)
  }

}
