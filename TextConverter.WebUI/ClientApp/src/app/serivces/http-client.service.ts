import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { baseUrl } from '../helpers/constants';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  headers: HttpHeaders;
  private baseUrl: string;

  constructor(private http: HttpClient, @Inject(baseUrl) basePath: string) {
    this.baseUrl = basePath;
    this.headers = new HttpHeaders({
      "Content-Type": "application/json",
    })
  }

  post<T>(url: string, body: any): Observable<T> {
    return this.http.post<T>(this.baseUrl + url, body, { headers: this.headers });
  }
}
