﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TextConverter.DataAccessLayer.DTO;
using TextConverter.Services.TextConverterService;

namespace TextConverter.WebUI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConverterController : ControllerBase
    {
        private ITextConverterService _textConverterService;
        private readonly ILogger _logger;
        public ConverterController(ITextConverterService textConverterService, ILoggerFactory loggerFactory)
        {
            _textConverterService = textConverterService ?? throw new Exception($"{nameof(textConverterService)} is null. Did you forget to register it in DI container?");
            _logger = loggerFactory.CreateLogger(this.GetType());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ContentDTO content)
        {
            _logger.LogDebug("Post");
            var response = await _textConverterService.ConvertAsync(content);
            if (response.StatusCode != StatusCodes.Status200OK)
            {
                return Problem(response.ResultMessage);
            }

            return Ok(response.Data);
        }
    }
}
